# Test-driven Developed todo-list application

The application is quite simple itself. But it's built with TDD in mind.

High lights:

* test driven
* well tested
* functional test with selenium
* database migration support
* automated deploy & upgrade
* dev staging production env isolation